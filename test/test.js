const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
  });
}); 

describe('painters', function() {
  it('has paolo veronese in the list', function(done) {
    request(app)
      .get('/painters')
      .expect(/paolo veronese/i, done);
  });
}); 

describe('sculptors', function() {
  it('has phidias in the list', function(done) {
    request(app)
      .get('/sculptors')
      .expect(/phidias/i, done);
  });
}); 